all: build run
	
build:
	docker build -t registry.gitlab.com/belligerence/words_in_lyrics .
run:
	docker run -d --name lyric -p 443:8443 -p 80:8080 registry.gitlab.com/belligerence/words_in_lyrics
push:
	docker push registry.gitlab.com/belligerence/words_in_lyrics

clean:
	docker stop lyric
	docker rm lyric

